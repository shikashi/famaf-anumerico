function t = lab2ej5(R, x0, delta)
	max_it = 100;
	epsilon = 0.0000001;
	[k v] = newton_raphson(x0, max_it, delta, epsilon, @(x) x^3 - R, @(x) 3*(x^2));
	t = v(length(v));
endfunction

