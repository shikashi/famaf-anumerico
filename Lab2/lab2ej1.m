# see bisection.m
# example:

f = @(x) x^3;
g = @(x) tan(x) - 2*x;

bisection(-1, 1.35, 0.0001, f);
