# this is a function file
function lab2ej2c()
	[k c] = bisection(1, 2, 0.00001, @(x) x^2 - 3, false);
	aprox = c(length(c));

	# find (x, x^2 - 3) pairs to be plotted
	x = 1:0.01:2;
	t = 1;
	for i = 1:0.01:2
		y(t) = i^2 - 3;
		t++;
	end

	plot(x, y, [aprox aprox], [-3 5]); # plot x^2 - 3 and the aproximation to sqrt(3) found
	axis([1 2 -3 5]);
	grid("on");
	title(strcat("Comparison | Aproximation to sqrt(3) is: ", num2str(aprox, 16)));
endfunction

