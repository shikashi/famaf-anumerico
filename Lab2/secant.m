## secant(a, b, M, delta, epsilon, func)
##	a: second to last aproximation
##	b: last aproximation
##	N: maximum number of iterations
##	delta: aproximation threshold
##	epsilon: functional threshold
##	func: higher-order function to call
function [Vk Vx] = secant(a, b, N, delta, epsilon, func)
	printf("Aproximación penúltima: %.12g\n", a);
	printf("Aproximación última: %.12g\n", b);
	printf("Iteraciones máximas: %d\n", N);
	printf("Umbral de aproximación: %.12g\n", delta);
	printf("Umbral funcional: %.12g\n", epsilon);

	u = func(a);
	v = func(b);

	Vk = [1 2];
	Vx = [a b];

	printf(" Iteración |    Aproximación    | f en aproximación  | Aprox. al error relativo \n");
	printf("%10d | %18.12g | %18.12g |\n", 0, a, u);
	printf("%10d | %18.12g | %18.12g | %18.12g\n", 1, b, v, (b - a) / b);
	
	for k = 1:N
		slope = (b - a) / (v - u);
		a = b;
		u = v;
		b = b - v*slope;
		v = func(b);
		#eabs = abs(b - a);
		erel = abs((b - a)/b);

		Vk(k+2) = k;
		Vx(k+2) = b;

		printf("%10d | %18.12g | %18.12g | %18.12g\n", k+2, b, v, erel);

		if abs(v) < epsilon || erel < delta
			printf("Aproximación hallada: %.12g\n", b);
			break;
		endif
	endfor
endfunction

