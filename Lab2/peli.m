% Este programa hace una "pelicula" de la solución de la ecuación de ondas 
% dada por la formula de D'Alembert.
% La formula de D'Alembert
% 	u(x,t) = F(x-c*t) + G(x+c*t)
% resuelve la ecuación diferencial
% 	u_tt = c^2 u_xx
% En este caso la funciones F y G son conocidas y están dadas por:
%	F(x) = exp(-abs(x)) 
%	G(x) = 0.2*sin(x)

c = 1;                    % velocidad = 1
G = @(x) exp(-abs(x));    % define la función G
F = @(x) 0.2*sin(x);      % define la función F

% plot movie
x = -15:0.05:15;          % define una partición del eje x de 600 puntos
for t=0:0.1:5
	u = F(x-c*t) + G(x+c*t);      % calcula la solución para el tiempo t
 	plot(x,u)					  % grafica dicha solución
  	axis([-15 15 -0.5 1.3]);	  % fija los ejes x e y
  	title(sprintf('t = %4.2f',t)) % pone como titulo del grafico el tiempo t
  	pause(0.05)                   % hace una pausa de modo que se vean 20 cuadros por segundo
end

