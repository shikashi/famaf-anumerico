function [Vk Vx Vf] = regula_falsi(a, b, N, delta, func)
	printf("Intervalo inicial: [%f, %f]\n", a, b);
	printf("Iteraciones máximas: %d\n", N);
	printf("Umbral de aproximación: %.12g\n", delta);
	#printf("Umbral funcional: %.12g\n", epsilon);

	Vk = [];
	Vx = [];
	Vf = [];

	fa = func(a);
	fb = func(b);
	
	printf(" Iteración |                 Intervalo                |   Punto interior   |   f en pto. interior\n");

	for k = 1:N
		c = (fb*a - fa*b)/(fb - fa);
		fc = func(c);

		Vk(k) = k;
		Vx(k) = c;
		Vf(k) = fc;

		printf("%10d | [%18.12g, %18.12g] | %18.12g | %18.12g\n", k-1, a, b, c, fc);

		#if abs(c - a) < delta || abs(fc) < epsilon
		if abs(c - b) < delta
			printf("Aproximación hallada: %.12g\n", c);
			printf("Iteraciones realizadas: %.12g\n", k);
			break;
		endif
		if sign(fa) != sign(fc)
			b = c;
			fb = fc;
		else
			a = c;
			fa = fc;
		endif
	endfor
endfunction

