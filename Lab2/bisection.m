## bisection(a, b, N, delta, epsilon, func, doplot)
##	a: lower bound
##	b: higher bound
##	delta: aproximation threshold
##	epsilon: functional threshold
##	func: higher-order function to call
##	doplot: whether or not to show a plot of the sucession
function [Vk Vx] = bisection(a, b, N, delta, epsilon, func, doplot = true)
	printf("Intervalo: [%f, %f]\n", a, b);
	printf("Iteraciones máximas: %d\n", N);
	printf("Umbral de aproximación: %.12g\n", delta);
	printf("Umbral funcional: %.12g\n", epsilon);

	u = func(a);
	v = func(b);
	d = b - a;
	
	if sign(u) == sign(v)
		error("¡No se satisfacen las condiciones del teorema de Bolzano!");
	endif

	printf(" Iteración |                 Intervalo                |   Media longitud   |      Bisección     |   f en bisección\n");

	Vk = [];
	Vx = [];

	for k = 1:N
		d /= 2;
		c = a + d;
		w = func(c);

		Vk(k) = k;
		Vx(k) = c;

		printf("%10d | [%18.12g, %18.12g] | %18.12g | %18.12g | %18.12g\n", k-1, a, b, d, c, w);
		if abs(w) < epsilon || abs(c - b) < delta
			printf("Aproximación hallada: %.12g\n", c);
			printf("Iteraciones realizadas: %.12g\n", k);
			break;
		endif
		if sign(u) != sign(w)
			b = c;
			v = w;
		else
			a = c;
			u = w;
		endif
	endfor

	if doplot
		plot(Vk, Vx);
		axis([1 k min(Vx) max(Vx)]);
		grid("on");
		title("bisection");
	endif
endfunction

