# this is a function file
function lab2ej2b()
	[k c] = bisection(0.8, 1.4, 0.00001, @(x) tan(x) - 2*x, false);
	aprox = c(length(c));

	x = 0.8:0.01:1.4;
	plot(x, 2*x, x, tan(x), [aprox aprox], [0 5]); # plot 2*x, tan(x) and the aproximation found
	axis([0.8 1.4 0 5]);
	grid("on");
	title(strcat("Comparison | Aproximation is: ", num2str(aprox, 16)));
endfunction

