## newton_raphson(x0, N, delta, epsilon, func)
##	x0: initial aproximation
##	N: maximum number of iterations
##	delta: aproximation threshold
##	epsilon: functional threshold
##	func: higher-order function to call
##	func_derivative: derivative of func, higher-order function
function [Vk Vx] = newton_raphson(x0, N, delta, epsilon, func, func_derivative)
	printf("Aproximación inicial: %.12g\n", x0);
	printf("Iteraciones máximas: %d\n", N);
	printf("Umbral de aproximación: %.12g\n", delta);
	printf("Umbral funcional: %.12g\n", epsilon);

	v = func(x0);

	Vk = [1];
	Vx = [x0];

	printf(" Iteración |    Aproximación    | f en aproximación  |   f' en aproximación   | Aprox. al error relativo \n");
	printf("%10d | %18.12g | %18.12g |                        | \n", 0, x0, v);

	if abs(v) < epsilon
		printf("Aproximación hallada: %.12g\n", x0);
		return;
	endif
	for k = 1:N
		deriv = func_derivative(x0);
		if abs(deriv) < epsilon
			error("La derivada es demasiado próxima a cero.");
		endif

		x1 = x0 - v/deriv;
		v = func(x1);
		erel = abs((x1 - x0)/x0);

		Vk(k+1) = k;
		Vx(k+1) = x1;

		printf("%10d | %18.12g | %18.12g |     %18.12g | %18.12g\n", k+1, x1, v, deriv, erel);

		if abs(v) < epsilon || erel < delta
			printf("Aproximación hallada: %.12g\n", x1);
			break;
		endif
		x0 = x1;
	endfor
endfunction

