function parcial1c
	bisection(0, 2, 50, 0.0000005, 0.1/10^20, @(x) x*sin(x)-1, false);
	regula_falsi(0, 2, 50, 0.0000005, @(x) x*sin(x) - 1);
endfunction

