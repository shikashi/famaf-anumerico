function [xmin,xmax,ymin,ymax]=puntos_c(f,x)
% El siguiente programa grafica la funcion f. Primero grafica 10 puntos 
% y luego para todos los valores de x
%
% ejecutar [xmin,xmax,ymin,ymax]=puntos_c(f,x)
%
%	f='funcion', un string con el nombre de la funcion a usar
%	x puntos donde se evaluara f para realizar la grafica

close 		% "limpia" los graficos
hold on		% permite graficar sobre la misma ventana ( figure )

y=feval(f,x);	%y(i)=f(x(i)), f debe poder evaluarse en vectores
xmax=max(x); xmin=min(x);	%valor maximo y minimo de x
ymax=max(y); ymin=min(y);	%valor maximo y minimo de f(x)

n=length(x);		% cantidad de elementos de x y de y
m=floor((n-1)/10)+1;	% espaciamiento necesario para tomar a lo sumo 10 elementos de un total de n

for i =1:m:n
	plot(x(i),y(i),'marker','*','markersize',10,'color',[1-i/n,0,i/n])				 
  	axis([xmin xmax ymin ymax]);	      
  	pause(0.5)                   
end

plot(x,y,"g")	% en escala RGB equivale a [0,1,0]
 	
hold off   
