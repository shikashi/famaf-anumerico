function aprox = newton_raphson_method(x0, max_ite, tol, g, derivg)

     printf("Aproximación inicial: %-17.10g ; Número máximo de iteraciones: %d ; Tolerancia: %.10g\n", x0, max_ite, tol);
      printf("No voy a derivar la función. Supongo que usted lo ha hecho correctamente y la ha intoducido como 5º argumento.\n");

     v = g(x0);
     continuar = true;

     if abs(v) < tol
        printf("%-17.10g es la aproximación buscada. 0 iteraciones realizadas\n", x0);
        continuar = false;
     endif
     printf("x0 = %-17.10g | g(x1) = %-17.10g | #iteraciones = 0\n", x0, v);
     if continuar == true
        for k = 1:max_ite
            x1 = x0 - (g(x0)/derivg(x0));
            v = g(x1);
            printf("x1 = %-17.10g | g(x1) = %-17.10g | #iteraciones = %d\n", x1, v, k);
            if abs(v) < tol
                printf("%-17.10g es la aproximación buscada. %d iteraciones realizadas\n", x1, k);
                break;
            endif
            x0 = x1;
        endfor
     endif
endfunction
