## alt1
for j=2:n
	for i=1:n-j
		c(i,j) = (c(i+1,j-1) - c(i, j-1)) / (x(i+j-1) - x(i))
	end
end

## alt2
for i=1:n
	d(i) = f(x(i)) # d(i) = c(1,1)
end
for j=1:n
	for i=j:n
		d(i) = (d(i) - d(i-1)) / ((x(i) - x(i-j))
	end
end

