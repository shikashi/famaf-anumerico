## newton_divdiff(x, y, z)
## Preconditions: length(x) == length(y)
## Use newton_divdiff_unified().
##	x: vector of values to interpolate
##	y: vector of function values to interpolate
##	z: vector of values in which to evaluate the interpolatory polinomial
function w = newton_divdiff(x, y, z)
	w = newton_divdiff_unified(x, y, z);
end

## newton_divdiff_unified(x, y, z)
## Preconditions: length(x) == length(y)
## This implementation integrates the calculus of all needed div. diffs.
## into a matrix to avoid recalculating any of them.
##	x: vector of values to interpolate
##	y: vector of function values to interpolate
##	z: vector of values in which to evaluate the interpolatory polinomial
function w = newton_divdiff_unified(x, y, z)
	xdim = length(x);

	# calculate the div. diffs.
	dd = [];
	for i = 1:xdim
		dd(i,1) = y(i); # initialize first column
	end
	# do the actual computation
	for j = 2:xdim
		for i = 1:xdim-j+1
			dd(i,j) = (dd(i+1,j-1) - dd(i,j-1)) / (x(i+j-1) - x(i));
		end
	end

	# eval the polynomial using newton's procedure
	w = [];
	for i = 1:length(z)
		eval = 0;
		for k = 1:xdim
			ddiff = dd(1,k);
			for j = 1:k-1
				ddiff *= z(i) - x(j);
			endfor
			eval += ddiff;
		endfor
		w(i) = eval;
	endfor
end

## newton_divdiff_delegated(x, y, z)
## Preconditions: length(x) == length(y)
## This implementation delegates the calculus of the div. diffs. and in so
## doing it re-calculates lots of them.
##	x: vector of values to interpolate
##	y: vector of function values to interpolate
##	z: vector of values in which to evaluate the interpolatory polinomial
function w = newton_divdiff_delegated(x, y, z)
	w = [];
	xdim = length(x);
	for i = 1:length(z)
		eval = 0;
		for k = 1:xdim
			ddiff = divdiff_ite(x(1:k), y(1:k));
			for j = 1:k-1
				ddiff *= z(i) - x(j);
			endfor
			eval += ddiff;
		endfor
		w(i) = eval;
	endfor
end

## divdiff_ite(xvec, yvec)
## Iterative implementation.
## Precondition: length(xvec) == length(yvec)
##	xvec: vector of abscissa values
##	yvec: vector of ordinate values corresponding to the ones in xvec
function r = divdiff_ite(xvec, yvec)
	# we will calculate the div. diffs. in yvec itself
	dim = length(xvec);
	for j = 1:dim-1
		for i = 1:dim-j
			yvec(i) = (yvec(i+1) - yvec(i)) / (xvec(i+j) - xvec(i));
		end
	end
	r = yvec(1);
end

## divdiff_rec(xvec, yvec)
## Precondition: length(xvec) == length(yvec)
## Recursive implementation. Innefficient.
##	xvec: vector of abscissa values
##	yvec: vector of ordinate values corresponding to the ones in xvec
function r = divdiff_rec(xvec, yvec)
	dim = length(xvec);
	if dim == 1
		r = yvec(1);
	elseif dim == 2
		r = (yvec(2) - yvec(1)) / (xvec(2) - xvec(1));
	elseif dim > 2
		r = (divdiff_rec(xvec(2:dim), yvec(2:dim)) - divdiff_rec(xvec(1:dim-1), yvec(1:dim-1))) / (xvec(dim) - xvec(1));
	end
end

