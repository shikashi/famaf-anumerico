function lab3ej5()
	load "TM.mat"

	data_year = [];
	data_tm = [];
	interp_year = [];
	j = 1; # data_* index
	k = 1; # interp_year index
	for i = 1:length(TM)
		if isna(TM(i,2))
			interp_year(k) = TM(i,1); # missing value, we will be interpolating this one
			k++;
		else
			data_year(j) = TM(i, 1);
			data_tm(j) = TM(i, 2);
			j++;
		end
	end

	interp_tm = interp1(data_year, data_tm, interp_year, "spline", "extrap");

	# update TM with the interpolate values
	k = 1; # interp_tm index
	for i = 1:length(TM)
		if isna(TM(i,2))
			TM(i,2) = interp_tm(k);
			k++;
		end
	end

	figure('Position', [-1,-1,1000,800]);
	plot(data_year, data_tm, "*", TM(:, 1), TM(:, 2));
	grid("on");
	title("Cubic spline interpolation");
end

