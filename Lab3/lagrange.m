## lagrange(x, y, z)
##	x: vector of values to interpolate
##	y: vector of function values to interpolate
##	z: vector of values in which to evaluate the interpolatory polinomial
function w = lagrange(x, y, z)
	w = [];
	xdim = length(x);
	for i = 1:length(z)
		eval = 0;
		for j = 1:xdim
			product = y(j);
			for k = 1:xdim # lk(zi)
				if k != j
					product *= (z(i) - x(k)) / (x(j) - x(k));
				endif
			endfor
			eval += product;
		endfor
		w(i) = eval;
	endfor
endfunction

