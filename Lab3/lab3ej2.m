function lab3ej2()
	x = [1 2 3 4 5];

	z = 1:0.04:5;
	
	w = lagrange(x, F(x), z);

	figure('Position', [-1,-1,1000,800]);
	plot(z, F(z), z, w);
	grid("on");
	title("Interpolation, Lagrange");
end

function y = F(x)
	for i=1:length(x)
		y(i) = 1 / x(i);
	end
end

