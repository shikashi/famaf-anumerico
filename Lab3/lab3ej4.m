function lab3ej4()
	x1 = get_interpolatory_points(5);
	x2 = get_interpolatory_points(10);
	x3 = get_interpolatory_points(15);

	z = -1:0.01:1; # 200 ascissa points

	w1 = newton_divdiff(x1, F(x1), z);
	w2 = newton_divdiff(x2, F(x2), z);
	w3 = newton_divdiff(x3, F(x3), z);

	figure('Position', [-1,-1,1000,800]);
	plot(z, F(z), ";f(x);", z, w1, ";P5(x);", z, w2, ";P10(x);", z, w3, ";P15(x);");
	grid("on");
	title("Interpolation, Newton");
end

function x = get_interpolatory_points(n)
	for i = 0:n
		x(i+1) = 2*i/n - 1;
	end
end

function y = F(x)
	for i = 1:length(x)
		y(i) = 1 / (1 + 25 * x(i)^2);
	end
end

