% fact

factorial(6)

function r = facf(x)
	r = 1;
	f = x;
	while f > 1
		r = r * f
		f = f - 1;
	end
endfunction
