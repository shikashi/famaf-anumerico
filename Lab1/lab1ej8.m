

function r = harmonic(n)
	r = 0;
	u = 1;
	while u <= n
		r = r + 1 / u;
		u = u + 1;
	end
endfunction

function r = harmonic_find_standstill()
	x = 1;
	prev = 0;
	curr = harmonic(x);
	while prev ~= curr
		x = x + 1;
		prev = curr;
		curr = harmonic(x)
	end
endfunction


