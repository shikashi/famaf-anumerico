% mala

function [x1, x2] = mala(a, b, c)
	
	x1 = (-b + sqrt( b^2 - 4 * a * c )) / (2 * a)
	
	x2 = (-b  - sqrt( b^2 - 4 * a * c )) / (2 * a)
	
endfunction

% buena

function [x1, x2] = buena(a, b, c)

	if b == 0 && c == 0
		x1 = 0
	else
		x1 = (2 * c) / (- b - sqrt(b^2 - 4 * a * c ))
	endif
	
	x2 = (-b  - sqrt( b^2 - 4 * a * c )) / (2 * a)

endfunction
