v = [1 4]

u = [2 3 4 6]

A = [4 0; 5 2]

B = [5 1 6; 4 1 8]

% a)
pi * v

% b)
v' * v

% c)
sqrt(v' * v)

% d)
% cambiado para posibilitar la operacion
v' * u

% e)
v .* u(2:3)

% f)
A * v'

% g)
A^ 2

% h)
A .* A

% i)
A * B

% j)
A .* B(1:2,2:3)
