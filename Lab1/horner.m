
function p = horner(coefs, x)
	p = 0;
	if length(coefs) > 0
		p = coefs(1);
		for i = 2:length(coefs)
			p = p*x + coefs(i);
		end
	endif
end
