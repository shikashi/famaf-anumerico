function x = sol_ecs_triang_sup(A, b)
        for i = 1:rows(A)
               x(i) = b(i);
               for j = 1:i-1
                       x(i) -= A(i,j)*x(j);
               endfor
        x(i) /= A(i,i);
        endfor
        x = x';
endfunction

function x = sol_ecs_triang_inf(A, b)
        i = rows(A);
        while i >= 1
               x(i) = b(i);
               j = rows(A);
               while j > i
                       x(i) -= A(i,j)*x(j);
                       j--;
               endwhile
        x(i) /= A(i,i);
        i--;
        endwhile
        x = x';
endfunction


function s = sol_ecs_lu(A, b)
        [L, U, P] = lu(A);
        x = sol_ecs_triang_sup(U, b);
        y = U * x;
        s = sol_ecs_triang_inf(L, y);
endfunction

