
function sol = upper_triangular_solve(A, b)
	# backward substitution
	sol = [];
	for i = length(A):-1:1
		sol(i) = b(i);
		for j = i+1:length(A)
			sol(i) -= A(i,j) * sol(j);
		end
		sol(i) /= A(i,i);
	end
end

