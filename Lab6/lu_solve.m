function x = lu_solve(A, b)
	[L, U] = lu(A);

	y = lower_triangular_solve(L, b);
	x = upper_triangular_solve(U, y);
end

