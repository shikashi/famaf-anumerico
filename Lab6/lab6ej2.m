function lab6ej2
	A = [ 4 -1  0 -1  0  0;
		 -1  4 -1  0 -1  0;
		  0 -1  4  0  0 -1;
		 -1  0  0  4 -1  0;
		  0 -1  0 -1  4 -1;
		  0  0 -1  0 -1  4 ];

	b1 = [1 1 1 0 0 0];

	b2 = [1 1 1 1 1 1];

	printf("A * x = b1 =>\n");
	x = lu_solve(A, b1);
	for i = 1:length(x)
		printf("%18.12g\n", x(i));
	end

	printf("A * x = b2 =>\n");
	x = lu_solve(A, b2);
	for i = 1:length(x)
		printf("%18.12g\n", x(i));
	end
end

