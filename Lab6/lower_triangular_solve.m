
function sol = lower_triangular_solve(A, b)
	# forward substitution
	sol = [];
	for i = 1:length(A)
		sol(i) = b(i);
		for j = 1:i-1
			sol(i) -= A(i,j) * sol(j);
		end
		sol(i) /= A(i,i);
	end
end

