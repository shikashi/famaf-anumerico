function int_exp_minus_x(n)
        printf("Voy a calcularle e^(-x) con %d intervalos usando reglas de Simpson, Trapecio y Punto Medio compuestas.\n", n);
        s = compound_simpson_rule_exp(0,1,n);
        t = compound_trapezium_rule_exp(0,1,n);
        m = compound_mid_point_rule_exp(0,1,n);
        printf("Aproximación con Simpson compuesto    : %-9.9g\n", s);
        printf("Aproximación con Trapecio compuesto   : %-9.9g\n", t);
        printf("Aproximación con Punto Medio compuesto: %-9.9g\n", m);
        printf("Valor \"real\"                          : 0.632120559\n");
endfunction

function s = compound_simpson_rule_exp(li, ls, n);
        h = (ls - li) / n;
        sx0 = exp(-li) + exp(-ls);
        sx1 = 0;
        sx2 = 0;
        for i=1:n-1
               x = li + i * h;
               if mod(i, 2) == 0
                       sx2 += exp(-x);
               else
                       sx1 += exp(-x);
               endif
        endfor
        s = sx0 + 2*sx2 + 4*sx1;
        s *= (h/3);
endfunction

function t = compound_trapezium_rule_exp(li, ls, n)
        h = (ls - li) / n;
        t = 0;
        for i=1:n-1
               x = li + h * i;
               t += exp(-x);
        endfor
        t *= 2
        t += exp(-li) + exp(-ls);
        t *= (h/2);
endfunction

function m = compound_mid_point_rule_exp(li, ls, n)
        h = (ls - li) / (n + 2);
        m = 0;
        for i=0:(n/2)
               x = li + (2*i + 1) * h;
               m += exp(-x);
        endfor
        m *= (2*h);
endfunction

