## int = trapezium_compound(F, a, b, n)
## Precondition: n > 0
##	F: higher-order function
##	a: integration interval lower limit
##	b: integration interval upper limit
##	n: quantity of subintervals in which to divide [a,b]
function int = trapezium_compound(F, a, b, n)
	x = get_interspersed_points(a, b, n+1);
	y = vector_eval(F, x);
	int = trapezium_compound_from_data(y, (b - a) / n);
end

## int = trapezium_compound_from_data(y, subintervals_length)
## Preconditions: length(y) >= 2
##	y: vector of function values
##	subintervals_length: length of the subintervals at which limits the
##	values of y where taken
function int = trapezium_compound_from_data(y, subintervals_length)
	n = length(y) - 1;
	int = y(1); # first point
	for i = 2:n
		int += 2 * y(i); # all-but-last pairs last point (equivalently, all-but-first pairs first point)
	end	
	int += y(n+1); # last point
	int *= subintervals_length/2;
end

