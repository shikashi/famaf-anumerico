function s = compound_simpson_rule(li, ls, n, f);
        h = (ls - li) / n;
        sx0 = f(li) + f(ls);
        sx1 = 0;
        sx2 = 0;
        for i=1:n-1
               x = li + i * h;
               if mod(i, 2) == 0
                       sx2 += f(x);
               else
                       sx1 += f(x);
               endif
        endfor
        s = sx0 + 2*sx2 + 4*sx1;
        s *= (h/3);
endfunction
function t = compound_trapezium_rule(li, ls, n, f)
        h = (ls - li) / n;
        t = 0;
        for i=1:n-1
               x = li + h * i;
               t += f(x);
        endfor
        t *= 2;
        t += f(li) + f(ls);
        t *= (h/2);
endfunction
function m = compound_mid_point_rule(li, ls, n, f)
        h = (ls - li) / (n + 2);
        m = 0;
        for i=0:(n/2)
               x = li + (2*i + 1) * h;
               m += f(x);
        endfor
        m *= (2*h);
endfunction

