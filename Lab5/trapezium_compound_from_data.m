## int = trapezium_compound_from_data(y, subintervals_length)
## Preconditions: length(y) >= 2
##	y: vector of function values
##	subintervals_length: length of the subintervals at which limits the
##	values of y where taken
function int = trapezium_compound_from_data(y, subintervals_length)
	n = length(y) - 1;
	int = y(1); # first point
	for i = 2:n
		int += 2 * y(i); # all-but-last pairs last point (equivalently, all-but-first pairs first point)
	end	
	int += y(n+1); # last point
	int *= subintervals_length/2;
end

