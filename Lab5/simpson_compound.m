## int = simpson_compound(F, a, b, n)
## Precondition:  n > 1 && n % 2 == 0
##	F: higher-order function
##	a: integration interval lower limit
##	b: integration interval upper limit
##	n: quantity of subintervals in which to divide [a,b]
function int = simpson_compound(F, a, b, n)
	x = get_interspersed_points(a, b, n+1);
	y = vector_eval(F, x);
	int = simpson_compound_from_data(y, (b - a) / n);
end

## int = simpson_compound_from_data(y, subintervals_length)
## Preconditions: length(y) % 2 == 1 && length(y) >= 3
##	y: vector of function values
##	subintervals_length: length of the subintervals at which limits the
##	values of y where taken
function int = simpson_compound_from_data(y, subintervals_length)
	n = length(y) - 1;
	int = y(1); # first point
	int += 4 * y(2); # first tercet mid point
	for i = 2:n/2
		int += 2 * y(2*i-1); # all-but-last tercets last point (equivalently, all-but-first tercets first point)
		int += 4 * y(2*i); # all-but-first tercets mid-point
	end	
	int += y(n+1); # last point
	int *= subintervals_length/3;
end

