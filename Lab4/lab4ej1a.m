function lab4ej1a()
	load "datos1a.mat";

	coefs = polyfit(xd, yd, 1);

	xp = min(xd):0.02:max(xd);
	#yp = coefs(1) * xp + coefs(2);
	yp = polyval(coefs, xp); # horner

	plot(xd, yd, "*", xp, yp);
	grid("on");
	title("Least squares aproximation, lineal");
end

