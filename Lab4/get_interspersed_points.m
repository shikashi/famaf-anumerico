## x = get_interspersed_points(a, b, n)
## Returns a vector of n interspersed points laying
## between a and b inclusive.
## Precondition: n >= 1
##	a: interval start
##	b: interval end
##	n: amount of points to return
function x = get_interspersed_points(a, b, n)
	if n < 2
		x = (a + b) / 2;
	else
		subintervals_length = (b - a) / (n - 1);
		for i = 0:n-1
			x(i+1) = a + subintervals_length * i;
		end
	end
end

