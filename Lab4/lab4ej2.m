function lab4ej2()
	# f(x) = arcsin(x)
	# interval = [0, 1]
	x = get_interspersed_points(0, 1, 50);
	y = vector_eval(@(a) asin(a), x);
	res = calculate_residues(x, y, 5);
	printf("f(x) = arcsin(x)\n");
	for i = 1:length(res)
		printf("\tDegree %d: %f\n", i-1, res(i));
	end

	# f(x) = cos(x)
	# interval = [0, 4*pi]
	x = get_interspersed_points(0, 4*pi, 50);
	y = vector_eval(@(a) cos(a), x);
	res = calculate_residues(x, y, 5);
	printf("f(x) = cos(x)\n");
	for i = 1:length(res)
		printf("\tDegree %d: %f\n", i-1, res(i));
	end
end

## residue = calculate_residues(x, y, max_fit_degree)
## Calculate the residue accumulation of aproximating the (x, y)
## pairs using least-squares aproximation with degrees from 0 to
## max_fit_degree. Returns a vector containing the accumulated 
## residues for each aproximation degree.
##	x: vector of abscissa values
##	y: vector of ordinate values
##	max_fit_degree: maximum degree of aproximation to test
function residue = calculate_residues(x, y, max_fit_degree)
	residue = [];
	for i = 0:max_fit_degree
		P = polyfit(x, y, i);
		residue(i+1) = 0;
		for j = 1:length(x)
			residue(i+1) += abs(polyval(P, x(j)) - y(j));
		end
	end
end
