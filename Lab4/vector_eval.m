## y = vector_eval(F, x)
## Evaluate F for each x and save the result in y.
##	F: higher order function
##	x: evaluation points
function y = vector_eval(F, x)
	y = [];
	for i = 1:length(x)
		y(i) = F(x(i));
	end
end

