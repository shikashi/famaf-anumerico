function lab4ej1b(epsilon)
	x = linspace(0, 10, 20);

	y = 3/4 * x - 1/2;
	y += epsilon * randn(1,20);

	coefs = polyfit(x, y, 1);

	xp = min(x):0.02:max(x);
	yp = polyval(coefs, xp); # horner

	plot(x, y, "*", xp, yp);
	grid("on");
	title(strcat("Least squares aproximation, lineal, white noise. Aprox: ",
			num2str(coefs(1), 8), " * x +", num2str(coefs(2), 8)));
end

