function x = gauss_seidel(A, b, x, delta, N)
	xdim = length(x);
	for it = 1:N
		xprev = x;
		for j = 1:xdim
			x(j) = b(j);
			for k = 1:xdim
				if k != j
					x(j) -= A(j, k) * x(k);
				end
			end
			x(j) /= A(j,j);
		end
		if norm(x - xprev) < delta
			printf("Iteraciones realizadas: %d\n", it);
			return;
		end
	end
	printf("Máxima cantidad de iteraciones alcanzada: %d\n", N);
end

