function [int_f, int_p] = parcial2ej1c(n)
	addpath("../Lab5");

	a = 0;
	b = 1;
	f = @(a) (1 + a^2)^(-1);
	[px, py] = parcial2ej1a(n, false);

	#interp = @(a) interp1(px, py, a, "linear");

	int_f = 4 / pi * trapezium_compound(f, a, b, n);
	int_p = 4 / pi * trapezium_compound_from_data(py, (b - a) / n);
	#int_p = 4 / pi * trapezium_compound(interp, a, b, n);
end

