function parcial2ej1d(n, max_test)
	addpath("../Lab5");

	a = 0;
	b = 1;
	f = @(a) (1 + a^2)^(-1);

	[px, py] = parcial2ej1a(n, false);
	interp = @(a) interp1(px, py, a, "linear");
	int_p = 4 / pi * trapezium_compound(interp, a, b, n);
	#int_p = 4 / pi * trapezium_compound_from_data(py, (b - a) / n);

	printf(" n (subintervalos) |      int f(x)      |       int p(x)     |     distancia\n");
	for i = 1:max_test
		int_f = 4 / pi * trapezium_compound(f, a, b, i);
		printf("%18d | %18.12g | %18.12g | %18.12g\n", i, int_f, int_p, abs(int_f - int_p));
	end
end

