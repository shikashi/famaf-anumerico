function [inter_x, inter_y] = parcial2ej1a(n, do_plot = true)
	addpath("../Lab4");

	a = 0;
	b = 1;
	f = @(a) (1 + a^2)^(-1);

	inter_x = get_interspersed_points(a, b, n+1);
	inter_y = vector_eval(f, inter_x);

#	k = 1;
#	for i = 1:n
#		P = interp1(inter_x, inter_y, "linear");
#	end

	if do_plot
		plot_x = a:0.02:b;
		plot_y = vector_eval(f, plot_x);
		inter_plot_y = interp1(inter_x, inter_y, plot_x, "linear");

		figure('Position', [-1,-1,1000,800]);
		plot(plot_x, plot_y, ";f(x);", plot_x, inter_plot_y, ";linear interpolation;");
		grid("on");
	end
end

