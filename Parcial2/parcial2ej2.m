function parcial2ej2(x0, delta, limit)
	A = [ 5 2 1;
		  2 7 1;
		  3 2 6 ];
	b = [ 4 ;
		  3 ;
		  4 ];

	printf("Gauss-Seidel:\n");
	gs = gauss_seidel(A, b, x0, delta, limit);
	for i = 1:length(gs)
		printf("\t%18.12g\n", gs(i));
	end

	printf("Gauss-Seidel Simétrico:\n");
	sgs = simetric_gauss_seidel(A, b, x0, delta, limit);
	for i = 1:length(sgs)
		printf("\t%18.12g\n", sgs(i));
	end
end

